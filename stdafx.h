// stdafx.h�: fichier Include pour les fichiers Include syst�me standard,
// ou les fichiers Include sp�cifiques aux projets qui sont utilis�s fr�quemment,
// et sont rarement modifi�s
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>
#include <iostream>
#include <iomanip>

#include "EDSDK.h"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"

#include <vector>
#include <queue>
#include <stack>
#include <list>
#include <map>
#include <functional>
#include <cmath>
#include <algorithm>

#define BOOST_FILESYSTEM_VERSION 3
#define BOOST_FILESYSTEM_NO_DEPRECATED 
#include <boost/filesystem.hpp>

using namespace std;
namespace fs = ::boost::filesystem;

#include "GSButton.h"
#include "GSCircle.h"
#include "GSWheel.h"
#include "GSChromaKey.h"
#include "OCVTools.h"


// TODO: faites r�f�rence ici aux en-t�tes suppl�mentaires n�cessaires au programme
