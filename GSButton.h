#pragma once

class GSButton;
class GSWheel;
typedef std::function<bool(GSWheel* parent, GSButton& button, int dx, int dy)> GSButtonMoveFunction;

class GSButton
{
    protected:
        cv::Point   m_pos;
        cv::Size    m_size;
        bool        m_isClicked;
        map<string, GSButton*>* m_collection;
        GSWheel*    m_parent;
        GSButtonMoveFunction* m_moveFunction;

    public:
        GSButton();
        GSButton(GSWheel* collection, const cv::Point& pos, bool centered = false, const cv::Size &size = cv::Size(-1, -1));
        ~GSButton();
        cv::Point   getPosition();
        cv::Point   getCentre();
        cv::Size    getSize();

        GSWheel* getParent() const{
            return m_parent;
        }
        map<string, GSButton*>* getCollection() const{
            return m_collection;
        }

        bool        render(cv::Mat& screen);
        void        click();
        void        unClick();
        bool        isClicked(const int &x, const int &y) const;
        bool        move(const int&x, const int &y, const bool &check = true);
        void        setMoveFunction(const GSButtonMoveFunction &func);
        void setX(int x){
            m_pos.x = x;
        }
        void setY(int y){
            m_pos.y = y;
        }

        static cv::Mat     s_img;
        static cv::Mat     s_imgClicked;
};

