#include "stdafx.h"

GSWheel::GSWheel(cv::Mat bg, cv::Point center, int radius)
        : GSCircle(center, radius), m_bg(bg), m_hue(0), m_deltaHue(30), m_sat(70), m_deltaSat(5), m_val(55), m_deltaVal(45)
        , m_cext(m_center, m_sat + m_deltaSat), m_cint(m_center, m_sat - m_deltaSat)
        , m_buttons(){
    m_render = cv::Mat(cv::Size(2 * (int)m_radius, 2 * (int)m_radius), CV_8UC4);
    m_cext = GSCircle(m_center, (int)(150 * (m_sat + m_deltaSat) / 100.));
    m_cint = GSCircle(m_center, (int)(150 * (m_sat - m_deltaSat) / 100.));
    setupButtons();
}

void GSWheel::setupButtons(){
    m_buttons["valMin"] = new GSButton(this, cv::Point(380, 200), true);
    m_buttons["valMax"] = new GSButton(this, cv::Point(380, 180), true);
    m_buttons["valMax"]->setMoveFunction(
        GSButtonMoveFunction(
            [&](GSWheel* parent, GSButton &button, int dx, int dy) -> bool {
                map<string, GSButton*>* buttons = parent->getButtons();
                GSButton* valMin = (*buttons)["valMin"];
                int x = button.getPosition().x;
                int y = button.getPosition().y;
                int x2 = x + dx;
                int y2 = y + dy;
                cv::Point pt(x2, y2);
                if (y2 > (valMin->getCentre().y - 10))
                    return false;
                button.move(380, max(50, y2), false);
                // calcul du nouveau val, maxVal, deltaVal
                double newMaxVal = 100.*(350. - max(50, y2)) / 300.;
                double currentValMin = parent->getVal() - parent->getDeltaVal();
                parent->setDeltaVal((newMaxVal - currentValMin) / 2.);
                parent->setVal(currentValMin + parent->getDeltaVal());
                return true;
            }
        )
    );
    m_buttons["valMax"]->move(380, 50, true);
    m_buttons["valMin"]->setMoveFunction(
        GSButtonMoveFunction(
            [&](GSWheel* parent, GSButton &button, int dx, int dy) -> bool {
                map<string, GSButton*>* buttons = parent->getButtons();
                GSButton* valMax = (*buttons)["valMax"];
                int x = button.getPosition().x;
                int y = button.getPosition().y;
                int x2 = x + dx;
                int y2 = y + dy;
                cv::Point pt(x2, y2);
                if (y2 < (valMax->getCentre().y + 10))
                    return false;
                button.move(380, min(350, y2), false);
                // calcul du nouveau val, minVal, deltaVal
                double newMinVal = 100.*(350. - min(350, y2)) / 300.;
                double currentValMax = parent->getVal() + parent->getDeltaVal();
                parent->setDeltaVal((currentValMax - newMinVal) / 2.);
                parent->setVal(currentValMax - parent->getDeltaVal());
                return true;
            }
        )
    );
    m_buttons["valMin"]->move(380, 200, true);

    m_buttons["hue"] = new GSButton(this, cv::Point(10, 200), true); // doit �tre d�tach� du bord
    m_buttons["hue"]->move(60, 200, false);
    m_buttons["hue"]->setMoveFunction(
        GSButtonMoveFunction(
            [&](GSWheel* parent, GSButton &button, int dx, int dy) -> bool {
                map<string, GSButton*>* buttons = parent->getButtons();
                int x = button.getPosition().x;
                int y = button.getPosition().y;
                int x2 = x + dx;
                int y2 = y + dy;
                cv::Point pt(x2, y2);
                double angle = angleTroisPoints(cv::Point(200, 0), cv::Point(200, 200), pt);
                if (angle > 360.)
                    angle -= 360;
                if (angle < 0.)
                    angle += 360;
                double angleRad = degToRad(angle);
                x2 = (int)(200 - 180.*sin(angleRad));
                y2 = (int)(200 - 180.*cos(angleRad));
                button.move(x2, y2, false);
                parent->setHue(angle);
                // delta teinte
                GSButton *hueSup = (*buttons)["hueSup"];
                angle += parent->getDeltaHue();
                if (angle > 360.)
                    angle -= 360;
                if (angle < 0.)
                    angle += 360;
                angleRad = angle * (3.14159 / 180.);
                x2 = (int)(200 - 150.*sin(angleRad));
                y2 = (int)(200 - 150.*cos(angleRad));
                hueSup->move(x2, y2, false);

                GSButton *hueInf = (*buttons)["hueInf"];
                angle -= 2 * parent->getDeltaHue();
                if (angle > 360.)
                    angle -= 360;
                if (angle < 0.)
                    angle += 360;
                angleRad = angle * (3.14159 / 180.);
                x2 = (int)(200 - 150.*sin(angleRad));
                y2 = (int)(200 - 150.*cos(angleRad));
                hueInf->move(x2, y2, false);

                // saturation
                GSButton *satSup = (*buttons)["satSup"];
                GSButton *satInf = (*buttons)["satInf"];
                angle += parent->getDeltaHue(); // onr ecentre sur Hue
                angleRad = angle * (3.14159 / 180.);
                double rayonSatSup = 150 * ((parent->getSat() + parent->getDeltaSat()) / 100.);
                x2 = (int)(200 - rayonSatSup*sin(angleRad));
                y2 = (int)(200 - rayonSatSup*cos(angleRad));
                satSup->move(x2, y2, false);
                double rayonSatInf = 150 * ((parent->getSat() - parent->getDeltaSat()) / 100.);
                x2 = (int)(200 - rayonSatInf*sin(angleRad));
                y2 = (int)(200 - rayonSatInf*cos(angleRad));
                satInf->move(x2, y2, false);

                return true;
            }
        )
    );
    m_buttons["hueSup"] = new GSButton(this, cv::Point(50, 210), true);
    m_buttons["hueSup"]->setMoveFunction(
        GSButtonMoveFunction(
            [&](GSWheel* parent, GSButton &button, int dx, int dy) -> bool {
                map<string, GSButton*>* buttons = parent->getButtons();
                int x = button.getPosition().x;
                int y = button.getPosition().y;
                int x2 = x + dx;
                int y2 = y + dy;
                cv::Point pt(x2, y2);
                double hue = parent->getHue();
                double angle = angleTroisPoints(cv::Point(200, 0), cv::Point(200, 200), pt);
                if (angle > 360.)
                    angle -= 360;
                if (angle < 0.)
                    angle += 360;
                if (angle < hue || abs(angle - hue) > 80)
                    return false;
                double angleRad = angle * (3.14159 / 180.);
                x2 = (int)(200 - 150.*sin(angleRad));
                y2 = (int)(200 - 150.*cos(angleRad));
                button.move(x2, y2, false);

                double deltaHue = angle - hue;
                parent->setDeltaHue(deltaHue);

                GSButton *hueInf = (*buttons)["hueInf"];

                angle = hue - deltaHue;
                //cout << "hue:" << hue << " inf : " << angle << endl << endl << endl << endl;
                angleRad = angle * (3.14159 / 180.);
                x2 = (int)(200 - 150.*sin(angleRad));
                y2 = (int)(200 - 150.*cos(angleRad));
                hueInf->move(x2, y2, false);

                return true;
            }
        )
    );
    m_buttons["hueInf"] = new GSButton(this, cv::Point(150, 190), true);
    m_buttons["hueInf"]->setMoveFunction(
        GSButtonMoveFunction(
            [&](GSWheel* parent, GSButton &button, int dx, int dy) -> bool {
                map<string, GSButton*>* buttons = parent->getButtons();
                int x = button.getPosition().x;
                int y = button.getPosition().y;
                int x2 = x + dx;
                int y2 = y + dy;
                cv::Point pt(x2, y2);
                double hue = parent->getHue();
                double angle = angleTroisPoints(cv::Point(200, 0), cv::Point(200, 200), pt);
                if (angle > 360.)
                    angle -= 360;
                if (angle < 0.)
                    angle += 360;
                if (angle > hue || abs(angle - hue) > 80)
                    return false;
                double angleRad = angle * (3.14159 / 180.);
                x2 = (int)(200 - 150.*sin(angleRad));
                y2 = (int)(200 - 150.*cos(angleRad));
                button.move(x2, y2, false);

                double deltaHue = hue - angle;
                parent->setDeltaHue(deltaHue);

                GSButton *hueSup = (*buttons)["hueSup"];
                angle = hue + deltaHue;
                if (angle > 360.)
                    angle -= 360;
                if (angle < 0.)
                    angle += 360;
                angleRad = angle * (3.14159 / 180.);
                x2 = (int)(200 - 150.*sin(angleRad));
                y2 = (int)(200 - 150.*cos(angleRad));
                hueSup->move(x2, y2, false);

                return true;
            }
        )
    );
    m_buttons["satSup"] = new GSButton(this, cv::Point(150, 200), true);
    m_buttons["satSup"]->setMoveFunction(
        GSButtonMoveFunction(
            [&](GSWheel* parent, GSButton &button, int dx, int dy) -> bool {
                map<string, GSButton*>* buttons = parent->getButtons();
                int x = button.getPosition().x;
                int y = button.getPosition().y;
                int x2 = x + dx;
                int y2 = y + dy;
                cv::Point pt(x2, y2);
                // calculer distance entre point dragg� (pt) et origine
                // placer le point dur le cercle de rayon calcul� ligne dud essus � l'angle hue
                double distance = sqrt(pow(x2 - 200, 2) + pow(y2 - 200, 2));
                distance = min(distance, 150.);
                double satSup = 100. * (distance / 150.);
                double satInf = parent->getSat() - parent->getDeltaSat();
                if (satSup <= satInf+5)
                    return false;
                double angle = parent->getHue();
                double angleRad = parent->getHue() * (3.14159 / 180.);
                x2 = (int)(200 - distance*sin(angleRad));
                y2 = (int)(200 - distance*cos(angleRad));
                button.move(x2, y2, false);

                double nvDeltaSat = 0.5*(satSup - satInf);
                parent->setSat(satInf + nvDeltaSat, nvDeltaSat);
                return true;
            }
        )
    );
    m_buttons["satInf"] = new GSButton(this, cv::Point(170, 200), true);
    m_buttons["satInf"]->setMoveFunction(
        GSButtonMoveFunction(
            [&](GSWheel* parent, GSButton &button, int dx, int dy) -> bool {
                map<string, GSButton*>* buttons = parent->getButtons();
                int x = button.getPosition().x;
                int y = button.getPosition().y;
                int x2 = x + dx;
                int y2 = y + dy;
                cv::Point pt(x2, y2);
                // calculer distance entre point dragg� (pt) et origine
                // placer le point dur le cercle de rayon calcul� ligne dud essus � l'angle hue
                double distance = sqrt(pow(x2 - 200, 2) + pow(y2 - 200, 2));
                double satInf = 100. * (distance / 150.);
                double satSup = parent->getSat() + parent->getDeltaSat();
                if (satInf < 1 || (satSup - satInf) <= 5)
                    return false;
                double angleRad = parent->getHue() * (3.14159 / 180.);
                x2 = (int)(200 - distance*sin(angleRad));
                y2 = (int)(200 - distance*cos(angleRad));
                button.move(x2, y2, false);

                double nvDeltaSat = 0.5*(satSup - satInf);
                parent->setSat(satInf + nvDeltaSat, nvDeltaSat);
                return true;
            }
        )
    );
    m_buttons["hue"]->move(60, 300, true);
}

GSWheel::~GSWheel(){
    for (map<string, GSButton*>::iterator it = m_buttons.begin(); it != m_buttons.end(); it++)
        delete it->second;
}

void effacerImg(cv::Mat& img){
    uchar* row0 = img.ptr<uchar>(0);
    uchar pixel[4] = { 0, 0, 0, 0};
    int nci = img.channels();
    for (int i = 0; i < img.cols; i++)
        memcpy(row0 + i*nci, pixel, nci);
    for (int i = 0; i < img.rows; i++)
        memcpy(img.ptr<uchar>(i), row0, img.cols*nci);
}
void GSWheel::render(){
    int minDim = (2 * (int)m_radius);
    assert(m_render.cols >= minDim || m_render.rows >= minDim);
    effacerImg(m_render);
    uchar pixel[4] = { 255, 0, 0, 255 };
    int nci = m_render.channels();
    m_cext.setRadius(150.*(m_sat + m_deltaSat) / 100.);
    m_cint.setRadius(150.*(m_sat - m_deltaSat) / 100.);
    double angleSup = m_hue + m_deltaHue;
    double angleInf = m_hue - m_deltaHue;
    if (angleSup > 360)
        angleSup -= 360;
    if (angleSup < 0)
        angleSup += 360;
    if (angleInf > 360)
        angleInf -= 360;
    if (angleInf < 0)
        angleInf += 360;
    //cout << " sup:" << angleSup << " inf:" << angleInf << endl;
    /*for (int i = 0; i < m_render.rows; i++)
        for (int j = 0; j < m_render.cols; j++){
            cv::Point p(i, j);
            double angle = -1 * angleTroisPoints(cv::Point((int)m_center.x, 0), m_center, p);
            angle += 90;
            if (angle > 360)
                angle -= 360;
            if (angle < 0)
                angle += 360;
            if (m_cext.appartientDisque(p) &&
                !m_cint.appartientDisque(p) && (
                    ( angleSup > angleInf && 
                      angle < angleSup &&
                      angle > angleInf) ||
                    ( angleSup < angleInf && (
                        angle < angleSup ||
                        angle > angleInf)))
                )
                memcpy(m_render.ptr<uchar>(i) +j*nci, pixel, nci);
        }*/
    cv::line(m_render, cv::Point(m_render.cols / 2, m_render.rows / 2), m_buttons["hue"]->getCentre(), cv::Scalar(27, 27, 27, 255), 1);
    cv::line(m_render, cv::Point(m_render.cols / 2, m_render.rows / 2), m_buttons["hueSup"]->getCentre(), cv::Scalar(27, 27, 27, 255), 1);
    cv::line(m_render, cv::Point(m_render.cols / 2, m_render.rows / 2), m_buttons["hueInf"]->getCentre(), cv::Scalar(27, 27, 27, 255), 1);
    for (map<string, GSButton*>::iterator it = m_buttons.begin(); it != m_buttons.end(); it++)
        it->second->render(m_render);
    std::ostringstream val, deltaVal, minVal, maxVal;
    val      << std::fixed << std::setprecision(2) << m_val;
    deltaVal << std::fixed << std::setprecision(2) << m_deltaVal;
    maxVal      << std::fixed << std::setprecision(2) << (m_val - m_deltaVal);
    minVal << std::fixed << std::setprecision(2) << (m_val + m_deltaVal);
    cv::putText(m_render, val.str(),      cv::Point(270, 15), CV_FONT_HERSHEY_COMPLEX_SMALL, .7, cv::Scalar(0, 0, 0, 255));
    cv::putText(m_render, "+/-",          cv::Point(315, 15), CV_FONT_HERSHEY_COMPLEX_SMALL, .7, cv::Scalar(0, 0, 0, 255));
    cv::putText(m_render, deltaVal.str(), cv::Point(355, 15), CV_FONT_HERSHEY_COMPLEX_SMALL, .7, cv::Scalar(0, 0, 0, 255));
    cv::putText(m_render, minVal.str(),   cv::Point(260, 30), CV_FONT_HERSHEY_COMPLEX_SMALL, .7, cv::Scalar(0, 0, 0, 255));
    cv::putText(m_render, "<->",          cv::Point(315, 30), CV_FONT_HERSHEY_COMPLEX_SMALL, .7, cv::Scalar(0, 0, 0, 255));
    cv::putText(m_render, maxVal.str(),   cv::Point(355, 30), CV_FONT_HERSHEY_COMPLEX_SMALL, .7, cv::Scalar(0, 0, 0, 255));
    m_upToDate = true;
}
void GSWheel::draw(cv::Mat& screen){
    if (!m_upToDate)
        render();
    plaquage(screen, m_render, 100);
}
bool GSWheel::setHue(const double &hue){
    m_hue = hue;
    m_upToDate = false;
    return true;
}
bool GSWheel::setDeltaHue(const double &deltaHue){
    if ((m_hue + deltaHue) > 360. || (deltaHue + m_hue) < 0.)
        return false;
    m_deltaHue = deltaHue;
    m_upToDate = false;
    return true;
}
bool GSWheel::setSat(const double &sat){
    if ((sat + m_deltaSat) > 100. || (sat + m_deltaSat) < 0.)
        return false;
    m_sat = sat;
    m_upToDate = false;
    return true;
}
bool GSWheel::setDeltaSat(const double &deltaSat){
    if ((m_sat + deltaSat) > 100. || (deltaSat + m_sat) < 0.)
        return false;
    m_deltaSat = deltaSat;
    m_upToDate = false;
    return true;
}
double GSWheel::getHue() const{
    return m_hue;
}
double GSWheel::getDeltaHue() const{
    return m_deltaHue;
}
double GSWheel::getSat() const{
    return m_sat;
}
double GSWheel::getDeltaSat() const{
    return m_deltaSat;
}
void GSWheel::clickCallback(int event, int x, int y, int flags, void* instance){
    ((GSWheel*)instance)->click(event, x, y, flags);
}
void GSWheel::click(int event, int x, int y, int flags){
    static bool mousePressed = false;
    static GSButton* clicked = nullptr;
    int padding = GSButton::s_img.rows / 2 + 1;
    x = min(x, (m_render.cols - padding));
    y = min(y, (m_render.rows - padding));
    x = max(padding, x);
    y = max(padding, y);

    switch (event){
        case CV_EVENT_LBUTTONDOWN:
            for (map<string, GSButton*>::iterator it = m_buttons.begin(); it != m_buttons.end(); it++){
                if (it->second->isClicked(x, y)){
                    it->second->click();
                    clicked = it->second;
                }
                else
                    it->second->unClick();
            }
            m_upToDate = false;
            mousePressed = true;
            break;
        case CV_EVENT_LBUTTONUP:
            for (map<string, GSButton*>::iterator it = m_buttons.begin(); it != m_buttons.end(); it++)
                it->second->unClick();
            mousePressed = false;
            clicked = nullptr;
            m_upToDate = false;
            break;
        case CV_EVENT_MOUSEMOVE:
            if (mousePressed && clicked)
                clicked->move(x, y);
            m_upToDate = false;
            break;
    }
}
bool GSWheel::setSat(const double &sat, const double &deltaSat){
    m_sat = sat;
    m_deltaSat = deltaSat;
    return true;
}
bool GSWheel::isUpToDate() const{
    return m_upToDate;
}
bool GSWheel::setVal(const double &val){
    m_val = val;
    return true;
}
bool GSWheel::setDeltaVal(const double &dVal){
    m_deltaVal = dVal;
    return true;
}
double GSWheel::getVal() const{
    return m_val;
}
double GSWheel::getDeltaVal() const{
    return m_deltaVal;
}