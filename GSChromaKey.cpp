#include "stdafx.h"


#ifndef max
#define max(a,b)            (((a) > (b)) ? (a) : (b))
#endif

#ifndef min
#define min(a,b)            (((a) < (b)) ? (a) : (b))
#endif

#pragma warning (disable : 4244)

GSChromaKey::GSChromaKey()
{
    m_hue = 120.0f;
    m_delta_hue = 30.0f;
    m_min_saturation = 0.2f;
    m_left_spill = m_right_spill = 0.0f;
    m_smoothing = 0.0f;

    m_min_value = .15f;
    m_max_value = 1.f;
}

GSChromaKey::~GSChromaKey()
{
}

void mixVal(float &valPixel, float valBg, float valMin, float valMax){
    double dist = 0.;
    if (valMin < valPixel && valPixel <= valMax){
        valPixel = valBg;// coef = .0;
        return;
    }
    // si on est un peu en dessous
    if (valPixel < valMin && (valPixel + .1) > valMin)
        dist = valMin - valPixel;
    // un peu au dessus
    else if  (valPixel > valMax && (valPixel - .1) <= valMax)
        dist = valPixel - valMax;
    else
        return;
    double coef = dist / (10. / 100.); // combien de premier plan met-on dans le pixel ?
    valPixel = coef*valPixel + (1 - coef)*valBg;
}
void mixHue(float &huePixel, float hueBg, float hueMin, float hueMax){
    if (hueMin < huePixel && huePixel < hueMax){ // condition principale sans quoi rien n'est fait sur le pixel
        huePixel = hueBg;
        return;
    }
    double dist = 0.;
    // si un petit peu en dessous niveau teinte (-5�)
    if (huePixel < hueMin && huePixel > (hueMin - 40. / 360.))
        dist = hueMin - huePixel;
    // si un petit peu au dessus niveau teinte (+5�)
    else if (huePixel > hueMax && huePixel <= (hueMax + 40. / 360.))
        dist = huePixel - hueMax;
    // si on est compl�tement en dehors de la zone de tol�rance
    else
        return;
    double coef = dist / (40. / 360.);
    huePixel = (1. - coef)*hueBg + coef*huePixel;
}
void mixSat(float &satPixel, float satBg, float satMin, float satMax){
    if (satMin < satPixel && satPixel < satMax){ // condition principale sans quoi rien n'est fait sur le pixel
        satPixel = satBg;
        return;
    }
    double dist = 0.;
    // si un petit peu en dessous niveau teinte (-5�)
    if (satPixel < satMin && satPixel > (satMin - 10. / 360.))
        dist = satMin - satPixel;
    // si un petit peu au dessus niveau teinte (+5�)
    else if (satPixel > satMax && satPixel <= (satMax + 10. / 360.))
        dist = satPixel - satMax;
    // si on est compl�tement en dehors de la zone de tol�rance
    else
        return;
    double coef = dist / (10. / 360.);
    if (coef > 1.)
        cout << "krke";
    if (coef < 0.)
        cout << "krke";
    satPixel = (1. - coef)*satBg + coef*satPixel;
}
// background & img doivent �tre en HSV
void GSChromaKey::process(const cv::Mat &background, cv::Mat& img, const GSWheel &settings, uchar **processedPixels)
{
    int nci = img.channels();
    int ncb = background.channels();

    double satMax = (settings.getSat() + settings.getDeltaSat()) / 100.;
    double satMin = (settings.getSat() - settings.getDeltaSat()) / 100.;

    double hueMax = (settings.getHue() + settings.getDeltaHue()) / 360.;
    double hueMin = (settings.getHue() - settings.getDeltaHue()) / 360.;

    double valMax = (settings.getVal() + settings.getDeltaVal()) / 100.;
    double valMin = (settings.getVal() - settings.getDeltaVal()) / 100.;

    //valMax = 1.;
    //valMin = .15;

    for (int i = 0; i < img.rows; i++){
        float* row = img.ptr<float>(i);
        const float* rowb = background.ptr<float>(i);
        for (int j = 0; j < img.cols; j++){
            float &hue = row[j*nci];
            float &sat = row[j*nci + 1];
            float &val = row[j*nci + 2];
            const float &hueb = rowb[j*ncb];
            const float &satb = rowb[j*ncb + 1];
            const float &valb = rowb[j*ncb + 2];
            if (processedPixels != nullptr)
                processedPixels[i][j] = 0;

            // booster la saturation (screen gain)
            float satBoosted = sat * 1.5;
            if (hueMin < hue && hue < hueMax){ // condition principale sans quoi rien n'est fait sur le pixel
                //if (satMin < sat/*Boosted*/ && sat <= satMax){
                    //if (valMin < val && val <= valMax){
                        hue = hueb;
                        processedPixels[i][j] = 255;
                        sat = satb;
                        //val = valb;
                        // mettre un signal sur le pixel pour signalzer au prichain algo qu'on aura pas de traitement � fairedessus
                        //if (processedPixels != nullptr)
                        //    processedPixels[i][j] = 255;
                       // //mixVal(val, valb, valMin*1.3, valMax);
                    //}
                //}
            }
            // si un petit peu en dessous niveau teinte (-5�)
            /*else if (hue < hueMin && hue >= (hueMin-5./360.)){

                // on ne peut faire �a que si on esr sur d"�tre dans lea bonne teine
                // impossible � d�rerminer si trop ptocje du noir
                // c'est ici qu'intervient le param�tre saturation
                // il faut que la satueation soit sup�rieure � la valeur min
                // et inf�rieure � la valeur max
                //mixHue(hue, hueb, hueMin, hueMax);
                if (satMin < sat && sat < satMax){
                    //hue = hueb;
                    mixHue(hue, hueb, hueMin, hueMax);
                    //mixSat(sat, satb, satMin, satMax);
                    //mixVal(val, valb, valMin, valMax);
                }
            }
            // si un petit peu en dessus niveau teinte (+5�)
            else if (hue > hueMax && hue <= (hueMax + 5 / 360.)){
                //mixHue(hue, hueb, hueMin, hueMax);
                if (satMin < sat && sat < satMax){
                    //hue = hueb;
                    mixHue(hue, hueb, hueMin, hueMax);
                    //mixSat(sat, satb, satMin, satMax);
                    //mixVal(val, valb, valMin, valMax);
                }
             }*/
        }
    }
}

void GSChromaKey::bgr2hsv(cv::Mat &img){
    cv::Mat result(cv::Size(img.cols, img.rows), CV_32FC(img.channels()));
    int nci = img.channels();
    int ncr = result.channels();
    for (int i = 0; i < img.rows; i++){
        uchar* row = img.ptr<uchar>(i);
        float* rowr = result.ptr<float>(i);
        for (int j = 0; j < img.cols; j++){
            rowr[j*ncr] = row[j*nci] / 255.;
            rowr[j*ncr + 1] = row[j*nci + 1] / 255.;
            rowr[j*ncr + 2] = row[j*nci + 2] / 255.;
            if (min(nci, ncr) == 4)
                rowr[j*ncr + 3] = row[j*nci + 3] / 255.;
            BGR_to_HSV(&(rowr[j*ncr]), &(rowr[j*ncr]), min(nci, ncr) == 4);
        };
    }
    img = result;
}
void GSChromaKey::hsv2bgr(cv::Mat &img){
    int nci = img.channels();
    cv::Mat result(cv::Size(img.cols, img.rows), CV_8UC(nci));
    for (int i = 0; i < img.rows; i++){
        uchar* rowr = result.ptr<uchar>(i);
        float* row = img.ptr<float>(i);
        for (int j = 0; j < img.cols; j++){
            GSChromaKey::HSV_to_BGR(&(row[j*nci]), &(row[j*nci]), nci == 4);
            rowr[j*nci]     = row[j*nci] * 255.;
            rowr[j*nci + 1] = row[j*nci + 1] * 255.;
            rowr[j*nci + 2] = row[j*nci + 2] * 255.;
            if (nci == 4)
                rowr[j*nci + 3] = row[j*nci + 3] * 255.;
        };
    }
    img = result;
}

void GSChromaKey::processOld(cv::Mat& img, const GSWheel &settings)
{
    m_hue = settings.getHue();
    m_delta_hue = 2 * settings.getDeltaHue();
    m_min_saturation = (settings.getSat() - settings.getDeltaSat()) / 100.;
    float max_saturation = (settings.getSat() + settings.getDeltaSat()) / 100.;

    m_min_value = (settings.getVal() - settings.getDeltaVal()) / 100.;
    m_max_value = (settings.getVal() + settings.getDeltaVal()) / 100.;
    m_smoothing = 1.;


    int height = img.rows;
    int width = img.cols;
    float BGR[4];
    float hsv[4];

    float h1 = m_hue - m_delta_hue / 2.0;
    float h2 = m_hue + m_delta_hue / 2.0;

    float smoothing = 1.0 - m_smoothing;

    float hue_tolerance = m_delta_hue / 2.0;
    hue_tolerance /= 360.0f;

    h1 /= 360.0f;
    h2 /= 360.0f;

    float s = m_min_saturation;
    float sSup = max_saturation;

    h1 -= 0.1f * smoothing;
    h2 += 0.1f * smoothing;

    int nci = img.channels();
    unsigned char *bits = 0;

    for (int y = 0; y < height; y++){
        bits = img.ptr<uchar>(y);
        for (int x = 0; x < width; x++){
            BGR[0] = *(bits + x * nci + 0) / 255.0f;
            BGR[1] = *(bits + x * nci + 1) / 255.0f;
            BGR[2] = *(bits + x * nci + 2) / 255.0f;
            if (nci == 4)
                BGR[3] = *(bits + x * 4 + 3) / 255.0f;

            BGR_to_HSV(BGR, hsv, (nci == 4));

            if (hsv[0] >= h1 && hsv[0] <= h2){ // si on est dans l'intervalle de la teinte
                if (hsv[1] >= s && sSup >= hsv[1]){ // si on est dans l'nitervalle de saturation
                    if (hsv[2] >= m_min_value && hsv[2] <= m_max_value){ // si on est dans l'intervalle de la valeur
                        if (nci == 4)
                            hsv[3] = 0.0f; // on rend transparent
                        hsv[1] = 0.0f;
                    }else if (hsv[2] < m_min_value){
                        // make an attempt to preserve the shadows
                        if (nci == 4)
                            hsv[3] = min(1, m_min_value + 1.0f - (hsv[2] / m_min_value));
                        hsv[1] = 0.0f;
                        hsv[2] = 0.0f;
                    }else if (hsv[2] > m_max_value){
                        // make an attempt to preserve the highlights
                        if (nci == 4)
                            hsv[3] = min(1, ((hsv[2] - m_max_value) / (1.0f - m_max_value)));
                        hsv[1] = 0.0f;
                        hsv[2] = 1.0f;
                    }
                    HSV_to_BGR(hsv, BGR, (nci == 4));
                }else{
                    if (nci == 4)
                        hsv[3] = 1.0f;
                    //hsv[1] = 0.0f;        //LA LIGNE DE MERDE
                    HSV_to_BGR(hsv, BGR, nci==4);
                }

                *(bits + x * nci + 0) = BGR[0] * 255.0f;
                *(bits + x * nci + 1) = BGR[1] * 255.0f;
                *(bits + x * nci + 2) = BGR[2] * 255.0f;
                if (nci == 4)
                    *(bits + x * 4 + 3) = BGR[3] * 255.0f;
            }
        }
    }
}

void GSChromaKey::BGR_to_HSV(float *color, float *hsv, bool alpha)
{
    float r, g, b, delta;
    float colorMax, colorMin;
    float h = 0, s = 0, v = 0;
    r = color[2];
    g = color[1];
    b = color[0];
    colorMax = max(r, g);
    colorMax = max(colorMax, b);
    colorMin = min(r, g);
    colorMin = min(colorMin, b);
    v = colorMax;

    if (colorMax != 0)
        s = (colorMax - colorMin) / colorMax;
    if (s != 0){ // if not achromatic
        delta = colorMax - colorMin;
        if (r == colorMax)
            h = (g - b) / delta;
        else if (g == colorMax)
            h = 2.0 + (b - r) / delta;
        else // b is max
            h = 4.0 + (r - g) / delta;
        h *= 60;

        if (h < 0)
            h += 360;
    }

    hsv[0] = h / 360.0; // moving h to be between 0 and 1.
    hsv[1] = s;
    hsv[2] = v;
    if (alpha)
        hsv[3] = color[3];
}

void GSChromaKey::HSV_to_BGR(float *hsv, float *bgr, bool alpha)
{
    float color[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
    float f, p, q, t;
    float h, s, v;
    float r = 0, g = 0, b = 0;
    float i;

    if (hsv[1] == 0){
        if (hsv[2] != 0)
            color[0] = color[1] = color[2] = color[3] = hsv[2];
    }
    else{
        h = hsv[0] * 360.0;
        s = hsv[1];
        v = hsv[2];

        if (h == 360.0)
            h = 0;

        h /= 60.0f;

        i = (float)((int)h);

        f = h - i;

        p = v *	(1.0 - s);
        q = v *	(1.0 - (s *	f));
        t = v *	(1.0 - (s *	(1.0 - f)));
        if (i < 0.01f){
            r = v;
            g = t;
            b = p;
        }else if (i < 1.01f){
            r = q;
            g = v;
            b = p;
        }else if (i < 2.01f){
            r = p;
            g = v;
            b = t;
        }else if (i < 3.01f){
            r = p;
            g = q;
            b = v;
        }else if (i < 4.01f){
            r = t;
            g = p;
            b = v;
        }else if (i < 5.01f){
            r = v;
            g = p;
            b = q;
        }

        color[0] = r;
        color[1] = g;
        color[2] = b;
    }
    if (alpha)
        color[3] = hsv[3];

    bgr[2] = color[0];
    bgr[1] = color[1];
    bgr[0] = color[2];
    if (alpha)
        bgr[3] = color[3];
}

