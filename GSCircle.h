#pragma once

class GSCircle
{
    protected:
        cv::Point_<double> m_center;
        double m_radius;
    public:
        GSCircle(cv::Point center, int radius) : m_center(center), m_radius(radius){}
        ~GSCircle(){}

        double a() const{ return m_center.x; }
        double b() const{ return m_center.y; }
        double r() const{ return m_radius; }
        double getRadius() const{ return m_radius; }
        cv::Point_<double> getCentre() const{ return m_center; }
        bool appartientDisque(cv::Point pt) const{
            cv::Point_<double> p(pt);
            double r = sqrt(pow((p.x - a()), 2) + pow((p.y - b()), 2));
            if (r < m_radius)
                return true;
            return false;
        }

        void setRadius(double r){
            m_radius = r;
        }
};

