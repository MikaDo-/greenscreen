#include "stdafx.h"

using namespace std;

cv::Mat GSButton::s_img = cv::Mat();
cv::Mat GSButton::s_imgClicked = cv::Mat();

GSButton::GSButton(){
}

GSButton::GSButton(GSWheel* parent, const cv::Point& pos, bool centered, const cv::Size &size)
        : m_pos(pos), m_size(size), m_isClicked(false), m_parent(parent), m_moveFunction(nullptr){
    if (size.width == -1 || size.height == -1){
        m_size.width = s_img.cols;
        m_size.height = s_img.rows;
    }
    if (centered){
        m_pos.x -= s_img.cols / 2;
        m_pos.y -= s_img.rows / 2;
    }
}

GSButton::~GSButton(){
    if (m_moveFunction != nullptr)
        delete m_moveFunction;
}

cv::Point GSButton::getPosition(){
    return m_pos;
}
cv::Point GSButton::getCentre(){
    return cv::Point(m_pos.x+s_img.cols/2, m_pos.y+s_img.rows/2);
}
cv::Size GSButton::getSize(){
    return cv::Size(s_img.cols, s_img.rows);
}
bool GSButton::render(cv::Mat& screen){
    cv::Mat& img = m_isClicked ? s_imgClicked : s_img;
    cv::Rect buttonRect(m_pos, cv::Size(img.cols, img.rows));
    cv::Rect screenRect(cv::Point(0, 0), cv::Size(screen.cols, screen.rows));
    cv::Rect ROI = buttonRect & screenRect;
    plaquage(screen(ROI), img, 100);
    return true;
}
bool GSButton::isClicked(const int &x, const int &y) const{
    return (x > m_pos.x && x < m_pos.x + m_size.width &&
        y > m_pos.y && y < m_pos.y + m_size.height);
}
void GSButton::click(){
    m_isClicked = true;
}
void GSButton::unClick(){
    m_isClicked = false;
}
bool GSButton::move(const int&x, const int &y, const bool& check){
    if (m_moveFunction != nullptr && check)
        return (*m_moveFunction)(m_parent, *this, x - m_pos.x, y - m_pos.y);
    else{
        m_pos.x = x - s_img.cols / 2;
        m_pos.y = y - s_img.rows / 2;

        m_pos.x = max(m_pos.x, 0);
        m_pos.y = max(m_pos.y, 0);
    }
    return true;
}
void GSButton::setMoveFunction(const GSButtonMoveFunction &func){
    if (func == nullptr)
        return;
    if (m_moveFunction != nullptr)
        delete m_moveFunction;
    m_moveFunction = new GSButtonMoveFunction(func);
}