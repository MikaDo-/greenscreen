#pragma once

class GSChromaKey
{
    public:
        GSChromaKey();
        virtual ~GSChromaKey();

        static void bgr2hsv(cv::Mat &img);
        static void hsv2bgr(cv::Mat &img);
        void processOld(cv::Mat& img, const GSWheel& settings);
        void process(const cv::Mat &background, cv::Mat& img, const GSWheel& settings, uchar** processedPixels);

        void setHue(float hue)					{ m_hue = hue; }
        float hue()								{ return m_hue; }

        void setTolerance(float tolerance)		{ m_delta_hue = tolerance; }
        float tolerance()						{ return m_delta_hue; }

        void setSaturation(float saturation)	{ m_min_saturation = saturation; }
        float saturation()						{ return m_min_saturation; }

        void setValue(float min, float max)		{ m_min_value = min; m_max_value = max; }
        float minValue()						{ return m_min_value; }
        float maxValue()						{ return m_max_value; }

        void setSpill(float left, float right)	{ m_left_spill = left; m_right_spill = right; }
        float leftSpill()						{ return m_left_spill; }
        float rightSpill()						{ return m_right_spill; }

    protected:
        float m_hue;
        float m_delta_hue;
        float m_min_saturation;
        float m_left_spill;
        float m_right_spill;

        float m_min_value;
        float m_max_value;

        float m_smoothing;



    public:
        static void BGR_to_HSV(float *color, float *hsv, bool alpha);
        static void HSV_to_BGR(float *hsv, float *bgr, bool alpha);

};