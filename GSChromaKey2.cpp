/* This is integer-only chroma key implementation. All warnings about
reliability of MMX version (see ck.c) apply here as well. This file
also contains several never used definitions (leftovers from earlier
versions) and I'm too lazy to clean this up. Look at ck.c for more
comments.
*/

#include "stdafx.h"
#include "GSChromaKey2.h"


GSChromaKey2::GSChromaKey2(cv::Mat fg, cv::Mat bg, float anglei, float noise_leveli){
    noise_level = noise_leveli;
    angle = anglei;
    if (fg.cols != bg.cols || fg.rows != bg.rows)
        return;
    width = bg.cols;
    height = bg.rows;
    memoryAllocated = false;
    bgy = (unsigned char *)calloc(width*height, sizeof(char));
    bgcr = (char *)calloc(width*height, sizeof(char));
    bgcb = (char *)calloc(width*height, sizeof(char));
    fgy = (unsigned char *)calloc(width*height, sizeof(char));
    fgcr = (char *)calloc(width*height, sizeof(char));
    fgcb = (char *)calloc(width*height, sizeof(char));
    resulty = (unsigned char *)calloc(width*height, sizeof(char));
    resultcb = (char *)calloc(width*height, sizeof(char));
    resultcr = (char *)calloc(width*height, sizeof(char));

    // masque final
    finalMask = cv::Mat(cv::Size(width, height), CV_8UC1);

    result = cv::Mat(cv::Size(width, height), CV_8UC3);
    fond = bg;
    devant = fg;

    if (bgy == NULL || fgy == NULL ||
        bgcb == NULL || fgcb == NULL ||
        bgcr == NULL || bgcr == NULL){
        printf("ERROR @GSChromaKey2::ctor - Memory allocation Failed. Exiting.\n");
        exit(1);
    }
    memoryAllocated = true;
    prepareInputs(cv::Point(0,0));
}
void GSChromaKey2::prepareInputs(cv::Point point){
    int nci = fond.channels();
    uchar r, g, b;
    for (int i = 0; i<height; i++){
        uchar* rowf = fond.ptr<uchar>(i);
        uchar* rowd = devant.ptr<uchar>(i);
        for (int j = 0; j<width; j++){
            b = rowd[j*nci + 0];
            g = rowd[j*nci + 1];
            r = rowd[j*nci + 2];
            /*if (i == point.y && j == point.x){
                ck_info.key_color.r = r;
                ck_info.key_color.g = g;
                ck_info.key_color.b = b;
                //printf("'%c', '%c', '%c'\n", r, g, b);
            }*/
            fgy[A(j, i)] = 0.257*r + 0.504*g + 0.098*b;
            fgcb[A(j, i)] = -0.148*r - 0.291*g + 0.439*b;
            fgcr[A(j, i)] = 0.439*r - 0.368*g - 0.071*b;
            b = rowf[j*nci + 0];
            g = rowf[j*nci + 1];
            r = rowf[j*nci + 2];
            bgy[A(j, i)] = 0.257*r + 0.504*g + 0.098*b;
            bgcb[A(j, i)] = -0.148*r - 0.291*g + 0.439*b;
            bgcr[A(j, i)] = 0.439*r - 0.368*g - 0.071*b;
        }
    }

    /*ck_info.key_color.r = 62;
    ck_info.key_color.g = 157;
    ck_info.key_color.b = 73;*/
    /*ck_info.key_color.r = 41;
    ck_info.key_color.g = 170;
    ck_info.key_color.b = 105;
    ck_info.key_color.r = 68;
    ck_info.key_color.g = 180;
    ck_info.key_color.b = 52;*/
    ck_info.key_color.r = 43;
    ck_info.key_color.g = 136;
    ck_info.key_color.b = 66;


    get_info(&ck_info);
}

void GSChromaKey2::setAngle(const float&mangle){
    angle = mangle;
    get_info(&ck_info);
}
void GSChromaKey2::prepareResult(){
    float tmp;
    int ncr = result.channels();
    for (int i = 0; i<height; i++){
        uchar* row = result.ptr<uchar>(i);
        for (int j = 0; j<width; j++){
            tmp = 1.164*resulty[A(j, i)] + 1.596*resultcr[A(j, i)];
            tmp = (tmp > 0) ? tmp : 0;
            row[j * ncr + 2] = (tmp < 255) ? tmp : 255;
            tmp = 1.164*resulty[A(j, i)] - 0.813*resultcr[A(j, i)] - 0.392*resultcb[A(j, i)];
            tmp = (tmp > 0) ? tmp : 0;
            row[j * ncr + 1] = (tmp < 255) ? tmp : 255;
            tmp = 1.164*resulty[A(j, i)] + 2.017*resultcb[A(j, i)];
            tmp = (tmp > 0) ? tmp : 0;
            row[j * ncr] = (tmp < 255) ? tmp : 255;
        }
    }
}

int GSChromaKey2::process(uchar **masque){
    int pixelsChanges = ck(fgy, fgcb, fgcr, bgy, bgcb, bgcr, resulty, resultcb, resultcr, &ck_info, masque);
    prepareResult();
    return pixelsChanges;
}

/* Main chroma key function.
Assumes global variables width and height are set correctly.
I was a bit paranoic about limits checking and explicit type
conversions. Some of them are not necessary - they are here to
get operation of this function as close as possible to MMX version.
This function was written first and MMX implementation from
ck.c is a translation of this function.
*/

int GSChromaKey2::ck(unsigned char *fgy, char *fgcb, char *fgcr,
    unsigned char *bgy, char *bgcb, char *bgcr,
    unsigned char *resy, char *rescb, char *rescr, info *cki, uchar **masque){
    unsigned i, j, r, g, b;
    int work_count = 0;
    float sp, kfg, kbg;
    float  alfa1, alfa2, alfa3, x1, x2, x3, y1, y2, y3;
    short tmp, tmp1;
    char x, z;

    for (i = 0; i<height; i++){
        uchar* finalMaskRow = finalMask.ptr<uchar>(i);
        for (j = 0; j<width; j++){
            if (masque != nullptr && masque[i][j] == 255){
                resy[A(j, i)] = fgy[A(j, i)];
                rescb[A(j, i)] = fgcb[A(j, i)];
                rescr[A(j, i)] = fgcr[A(j, i)];
                finalMaskRow[j] = 0;
            }
            /* Convert foreground to XZ coords where X direction is defined by
            the key color */

            tmp = ((short)(fgcb[A(j, i)])*cki->key_colorY.cb +
                (short)(fgcr[A(j, i)])*cki->key_colorY.cr) >> 7;   /*/128; */
            if (tmp > 127)
                tmp = 127;
            if (tmp < -128)
                tmp = -128;
            x = tmp;
            tmp = ((short)(fgcr[A(j, i)])*cki->key_colorY.cb -
                (short)(fgcb[A(j, i)])*cki->key_colorY.cr) >> 7;   /*/128; */
            if (tmp > 127) tmp = 127;
            if (tmp < -128) tmp = -128;
            z = tmp;

            /* WARNING: accept angle should never be set greater than "somewhat less
            than 90 degrees" to avoid dealing with negative/infinite tg. In reality,
            80 degrees should be enough if foreground is reasonable. If this seems
            to be a problem, go to alternative ways of checking point position
            (scalar product or line equations). This angle should not be too small
            either to avoid infinite ctg (used to suppress foreground without use of
            division)*/

            tmp = ((short)(x)*cki->accept_angle_tg) >> 4; /* /0x10; */
            if (tmp > 127) tmp = 127;
            if (abs(z) > tmp){
                /* keep foreground Kfg = 0*/
                resy[A(j, i)] = fgy[A(j, i)];
                rescb[A(j, i)] = fgcb[A(j, i)];
                rescr[A(j, i)] = fgcr[A(j, i)];
                /*resy[A(j,i)] = rescb[A(j,i)] = rescr[A(j,i)] = 0; */
                finalMaskRow[j] = 0;
            }
            else {
                work_count++;

                /* Compute Kfg (implicitly) and Kbg, suppress foreground in XZ coord
                according to Kfg */

                tmp = ((short)(z)*cki->accept_angle_ctg) >> 4; /*/0x10; */
                if (tmp > 127)
                    tmp = 127;
                if (tmp < -128) 
                    tmp = -128;
                x1 = abs(tmp);
                y1 = z;

                tmp1 = x - x1;
                if (tmp1 < 0)
                    tmp1 = 0;
                kbg = (((unsigned char)(tmp1)*(unsigned short)(cki->one_over_kc)) / 2);
                if (kbg < 0)
                    kbg = 0;
                if (kbg > 255)
                    kbg = 255;
                finalMaskRow[j] = kbg;
                tmp = ((unsigned short)(tmp1)*cki->kfgy_scale) >> 4;/* /0x10; */
                if (tmp > 0xFF)
                    tmp = 0xFF;
                tmp = fgy[A(j, i)] - tmp;
                if (tmp < 0)
                    tmp = 0;
                resy[A(j, i)] = tmp;

                /* Convert suppressed foreground back to CbCr */

                tmp = ((char)(x1)*(short)(cki->key_colorY.cb) -
                    (char)(y1)*(short)(cki->key_colorY.cr)) >> 7; /*/128; */
                if (tmp < -128)
                    rescb[A(j, i)] = -128;
                else if (tmp > 127)
                    rescb[A(j, i)] = 127;
                else
                    rescb[A(j, i)] = tmp;

                tmp = ((char)(x1)*(short)(cki->key_colorY.cr) +
                    (char)(y1)*(short)(cki->key_colorY.cb)) >> 7; /*/128; */
                if (tmp < -128)
                    rescr[A(j, i)] = -128;
                else if (tmp > 127)
                    rescr[A(j, i)] = 127;
                else
                    rescr[A(j, i)] = tmp;

                /* Deal with noise. For now, a circle around the key color with
                radius of noise_level treated as exact key color. Introduces
                sharp transitions.
                */

                tmp = z*(short)(z)+(x - cki->kg)*(short)(x - cki->kg);
                if (tmp > 0xFFFF)
                    tmp = 0xFFFF;
                if (tmp < noise_level*noise_level){
                    /* Uncomment this if you want total suppression within the noise circle */
                    //resy[A(j,i)]=rescb[A(j,i)]=rescr[A(j,i)]=0;
                    kbg = 255;
                }

                /* Add Kbg*background */

                tmp = resy[A(j, i)]*(255-kbg)/256 + ((unsigned short)(kbg)*bgy[A(j, i)]) / 256;
                resy[A(j, i)] = (tmp < 255) ? tmp : 255;
                tmp = rescb[A(j, i)] * (255 - kbg) / 256 + ((unsigned short)(kbg)*bgcb[A(j, i)]) / 256;
                if (tmp < -128)
                    rescb[A(j, i)] = -128;
                else if (tmp > 127)
                    rescb[A(j, i)] = 127;
                else
                    rescb[A(j, i)] = tmp;

                tmp = rescr[A(j, i)] * (255 - kbg) / 256 + ((unsigned short)(kbg)*bgcr[A(j, i)]) / 256;
                if (tmp < -128)
                    rescr[A(j, i)] = -128;
                else if (tmp > 127)
                    rescr[A(j, i)] = 127;
                else
                    rescr[A(j, i)] = tmp;
            }
        }
    }
    return work_count;
}

void GSChromaKey2::get_info(info *cki){
    float r, g, b, kgl;
    float tmp;
    kgl = GSChromaKey2::RGB2Y(&cki->key_color, &cki->key_colorY);
    cki->accept_angle_cos = cos(M_PI*angle / 180);
    cki->accept_angle_sin = sin(M_PI*angle / 180);
    tmp = 0xF * tan(M_PI*angle / 180);
    if (tmp > 0xFF) tmp = 0xFF;
    cki->accept_angle_tg = tmp;
    tmp = 0xF / tan(M_PI*angle / 180);
    if (tmp > 0xFF) tmp = 0xFF;
    cki->accept_angle_ctg = tmp;
    tmp = 1 / (kgl);
    cki->one_over_kc = 0xFF * 2 * tmp - 0xFF;
    tmp = 0xF * (float)(cki->key_colorY.y) / kgl;
    if (tmp > 0xFF) tmp = 0xFF;
    cki->kfgy_scale = tmp;
    if (kgl > 127) kgl = 127;
    cki->kg = kgl;
}
float GSChromaKey2::RGB2Y(color *c, colorY *y){
    float tmp, tmp1, tmp2;
    y->y = 0.257*c->r + 0.504*c->g + 0.098*c->b;
    tmp1 = -0.148*c->r - 0.291*c->g + 0.439*c->b;
    tmp2 = 0.439*c->r - 0.368*c->g - 0.071*c->b;
    tmp = sqrt(tmp1*tmp1 + tmp2*tmp2);
    y->cb = 127 * (tmp1 / tmp);
    y->cr = 127 * (tmp2 / tmp);
    return tmp;
}