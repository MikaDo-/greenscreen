#include "stdafx.h"
#include <ctime>
#include "GSChromaKey2.h"

void setupWindow(int xStart, int xEnd, int y, int w, int h, cv::Mat &screen){
    cv::namedWindow("colorwheel");
    HWND hWnd = FindWindow(NULL, L"colorwheel");
    MoveWindow(hWnd, xStart, y, w, h, true);
    screen = cv::Mat(cv::Size(w, h), CV_8UC4);
    cv::imshow("colorwheel", screen);
    cvWaitKey(1);
    for (int i = 0; i < 200; i++){
        MoveWindow(hWnd, xEnd + (int)((xEnd - xStart)*sin(i*3.14159 / 400)), y, 420, 420, true);
        Sleep(2);
    }

}

void getAllFiles(const fs::path& root, const string& ext, vector<fs::path>& ret){
    if (!fs::exists(root))
        return;
    if (fs::is_directory(root)){
        fs::recursive_directory_iterator it(root);
        fs::recursive_directory_iterator endit;
        while (it != endit){
            if (fs::is_regular_file(*it) && it->path().extension() == ext)
                ret.push_back(it->path().filename());
            ++it;
        }
    }
}

int _tmain2(int argc, _TCHAR* argv[]){
    list<cv::Mat> imgs;
    vector<fs::path> paths;
    getAllFiles("./", ".jpg", paths);
    for each (fs::path path in paths){
        cout << path.string() << endl;
        imgs.push_back(cv::imread(path.string()));
    }
    int widthMin = 10000, heightMin = 100000;
    for (list<cv::Mat>::iterator it = imgs.begin(); it != imgs.end();){
        if (it->dims != 2){
            list<cv::Mat>::iterator next = it++;
            imgs.erase(it);
            it = next;
            continue;
        }
        widthMin = min(widthMin, it->cols);
        heightMin = min(heightMin, it->rows);
        it++;
    }
    cv::Mat result(cv::Size(imgs.size() * 2 * widthMin, 2 * heightMin), CV_8UC3);
    cv::Mat bg = cv::imread("damier.png");
    bg = bg(cv::Rect((bg.cols - widthMin) / 2, (bg.rows - heightMin) / 2, widthMin, heightMin));



    GSWheel wheel(cv::Mat(), cv::Point(0, 0), 200); // on s'en fout
    wheel.setHue(130);
    wheel.setDeltaHue(40);
    wheel.setSat(73);
    wheel.setDeltaSat(27);

    int i = 0;
    for (list<cv::Mat>::iterator it = imgs.begin(); it != imgs.end(); it++, i++){
        if (it->cols > widthMin || it->rows > heightMin)
            (*it) = (*it)(cv::Rect((it->cols - widthMin) / 2, (it->rows - heightMin) / 2, widthMin, heightMin));
        // vierge
        cv::Rect ROI(widthMin * i * 2, 0, widthMin, heightMin);
        plaquage(result(ROI), *it, 100);
        cv::putText(result(ROI), "Original", cv::Point(20, 20), CV_FONT_HERSHEY_COMPLEX_SMALL, 1., cv::Scalar(0, 0, 255));
        // ---

        GSChromaKey key;
        // traitement 1
        ROI = cv::Rect(widthMin*(i * 2 + 1), 0, widthMin, heightMin);
        cv::Mat imgP;
        it->copyTo(imgP);
        addAlphaChannel(imgP);
        key.processOld(imgP, wheel); // imgP/4
        plaquage(result(ROI), bg, 100);
        plaquage(result(ROI), imgP, 100);
        cv::putText(result(ROI), "Version 1", cv::Point(20, 20), CV_FONT_HERSHEY_COMPLEX_SMALL, 1., cv::Scalar(0, 0, 255));
        // ---

        // traitement 2
        ROI = cv::Rect(widthMin * i * 2, heightMin, widthMin, heightMin);
        cv::Mat damierHSV = bg.clone();
        GSChromaKey::bgr2hsv(damierHSV);

        it->copyTo(imgP);
        GSChromaKey::bgr2hsv(imgP);

        key.process(damierHSV, imgP, wheel, nullptr);
        GSChromaKey::hsv2bgr(imgP);
        plaquageCentre(result(ROI), imgP, 100);
        cv::putText(result(ROI), "Version 2", cv::Point(20, 20), CV_FONT_HERSHEY_COMPLEX_SMALL, 1., cv::Scalar(0, 0, 255));
        removeAlphaChannel(*it);
        // ---

        // traitement 3
        ROI = cv::Rect(widthMin*(i * 2 + 1), heightMin, widthMin, heightMin);
        GSChromaKey2 ck(*it, bg, 80, 10.0);
        ck.process(nullptr);
        imgP = ck.result.clone(); // imgP/4
        plaquage(result(ROI), imgP, 100);
        cv::putText(result(ROI), "Version 3", cv::Point(20, 20), CV_FONT_HERSHEY_COMPLEX_SMALL, 1., cv::Scalar(0, 0, 255));
        // ---
    }
    imwrite("result.bmp", result);
    return 0;
}

int _tmain(int argc, _TCHAR* argv[]){
    cv::Mat screen;
    setupWindow(GetSystemMetrics(SM_CXSCREEN) + 10, GetSystemMetrics(SM_CXSCREEN) - 400, GetSystemMetrics(SM_CYSCREEN) - 400, 400, 400, screen);
    cv::Mat colorwheel      = cv::imread("colorwheel.png", -1);
    GSButton::s_img         = cv::imread("button.png", -1);
    GSButton::s_imgClicked  = cv::imread("buttonClicked.png", -1);
    cv::Mat img             = cv::imread("img.jpg", -1);
    cv::Mat damier          = cv::imread("damier.png");
    cv::Mat damierHSV       = cv::imread("damier.png");
    cv::Mat valueScale      = cv::imread("value.png");
    addAlphaChannel(damier);
    GSChromaKey::bgr2hsv(damierHSV);
    cv::Mat result(cv::Size(img.cols, img.rows), CV_8UC3);
    addAlphaChannel(img);
    cv::Mat imgP;
    if (colorwheel.dims != 2 || GSButton::s_img.dims != 2 || GSButton::s_imgClicked.dims != 2)
        return 2;
    GSWheel wheel(colorwheel, cv::Point(screen.cols / 2, screen.rows / 2), 200);
    cv::setMouseCallback("colorwheel", GSWheel::clickCallback, &wheel);

    /* Version YCbCr */
    GSChromaKey2 ck(img, damier, 80.f, 20.0f);

    bool colorWheelOnscreen = true;
    GSChromaKey key;
    // pr�parer tableau de bool pour masque algo1>algo2
    uchar **masque = (uchar**)malloc(img.rows * sizeof(uchar*));
    for (int i = 0; i < img.rows; i++)
        masque[i] = (uchar*)malloc(img.cols * sizeof(uchar));

    bool versionAlexis = true;
    bool versionFred = true;
    bool versionYCbCr = false;
    bool version3 = false;
    string version;
    while (colorWheelOnscreen){
        time_t debut = clock();
        img.copyTo(imgP);
        fillWith(screen, 120, 120, 120);
        plaquageCentre(screen, colorwheel, 100);
        plaquage(screen(cv::Rect(380-10, 50, 20, 300)), valueScale, 100);
        if (versionAlexis || versionFred || version3)
            plaquageCentre(result, damier, 100);
        if (versionAlexis){
            GSChromaKey::bgr2hsv(imgP);
            key.process(damierHSV, imgP, wheel, masque); //damierHSV/3 imgP/4
            GSChromaKey::hsv2bgr(imgP);
            version = "Algo 1 Alexis";
        }
        else if (versionFred){
            key.processOld(imgP, wheel); // imgP/4
            version = "Algo 1 Fred";
        }
        else if (versionYCbCr){
            ck.process(nullptr);
            imgP = ck.result.clone(); // imgP/4
            version = "Algo 2 YCbCr";
        }
        else if (version3){
            GSChromaKey::bgr2hsv(imgP);
            key.process(damierHSV, imgP, wheel, masque);
            GSChromaKey::hsv2bgr(imgP);
            GSChromaKey2 ck2(imgP, damier, 80.f, 20.0f);
            ck2.process(masque);
            imgP = ck2.result.clone();
            version = "Algo 3 MIX";
        }
        else
            version = "Aucun traitement.";

        plaquageCentre(result, imgP, 100);
        cv::putText(result, version, cv::Point(20, 20), CV_FONT_HERSHEY_COMPLEX_SMALL, 1., cv::Scalar(255, 255, 255));
        wheel.draw(screen);
        cv::imshow("colorwheel", screen);
        cv::imshow("result", result);
        cout << (clock() - debut) << "ms" << endl;

        // temp pour �crire mask
        cv::Mat mask(cv::Size(img.cols, img.rows), CV_8UC1);

        switch (cvWaitKey(5)){
            case 'w':
                cv::imwrite("result.bmp", result);
                removeAlphaChannel(imgP);
                ck.process(nullptr);
                cv::addWeighted(imgP, .5, ck.result.clone(), .5, .0, result);
                addAlphaChannel(imgP);
                cv::imwrite("resultMIX DE LEXTREME.bmp", result);
                break;
            case 'm':{
                for (int i = 0; i < img.rows; i++){
                    uchar* rowMask = mask.ptr<uchar>(i);
                    for (int j = 0; j < img.cols; j++)
                        rowMask[j] = masque[i][j];
                }
                cv::imwrite("maskAlexis.bmp", mask);

                key.processOld(imgP, wheel);
                cv::Mat maskFred(cv::Size(img.cols, img.rows), CV_8UC1);
                for (int i = 0; i < img.rows; i++){
                    uchar* rowMaskFred = maskFred.ptr<uchar>(i);
                    uchar* rowImgP = imgP.ptr<uchar>(i);
                    for (int j = 0; j < img.cols; j++)
                        rowMaskFred[j] = 255 - rowImgP[j*4 + 3];
                }

                cv::imwrite("maskFred.bmp", maskFred);
                cv::imwrite("finalMask.bmp", ck.finalMask);
                break;
                }
            case 'd':
                versionAlexis = true;
                versionFred = false;
                versionYCbCr = false;
                version3 = false;
                break;
            case 'f':
                versionAlexis = false;
                versionFred = true;
                versionYCbCr = false;
                version3 = false;
                break;
            case 'g':
                versionAlexis = false;
                versionFred = false;
                versionYCbCr = true;
                version3 = false;
                break;
            case 'h':
                versionAlexis = false;
                versionFred = false;
                versionYCbCr = false;
                version3 = true;
                break;
            case 'j':
                versionAlexis = false;
                versionFred = false;
                versionYCbCr = false;
                version3 = false;
                break;
            case 'q':
                colorWheelOnscreen = false;
                break;
        }
    }
    
	return EXIT_SUCCESS;
}

int ain(int argc, char* argv[]){
    cv::Mat img = cv::imread("img.jpg");
    cv::cvtColor(img, img, CV_BGR2HLS_FULL);
    //GSChromaKey::bgr2hsv(img);
    /*for (int i = 0; i < img.rows; i++){
        uchar* row = img.ptr<uchar>(i);
        for (int j = 0; j < img.cols; j++)
            row[j*img.channels() + 1] += 10;
    }
    //GSChromaKey::hsv2bgr(img);
    cv::cvtColor(img, img, CV_HLS2BGR_FULL);*/
    cv::imshow("result", img);
    cv::waitKey(0);
    return 0;
}