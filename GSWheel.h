#pragma once

class GSWheel : public GSCircle
{
    private:
        cv::Mat     m_bg;
        cv::Size    m_rect;
        cv::Mat     m_render;

        GSCircle    m_cint;
        GSCircle    m_cext;

        map<string, GSButton*>  m_buttons;

        double      m_hue;      // 0 <-> 360
        double      m_deltaHue; // 0 <->  30
        double      m_sat;      // 0 <-> 100
        double      m_deltaSat; // 0 <-> 50
        double      m_val;      // 0 <-> 100
        double      m_deltaVal; // 0 <-> 50

        bool        m_upToDate;
    public:
        GSWheel(cv::Mat bg, cv::Point center, int radius);
        ~GSWheel();

        void setupButtons();
        void render();
        void draw(cv::Mat &screen);
        static void clickCallback(int event, int x, int y, int, void* instance);
        void click(int event, int x, int y, int flags);
        bool isUpToDate() const;

        map<string, GSButton*>* getButtons(){
            return &m_buttons;
        }

        bool setHue(const double &hue);
        bool setDeltaHue(const double &deltaHue);
        bool setSat(const double &sat);
        bool setSat(const double &sat, const double &deltaSat);
        bool setDeltaSat(const double &);
        bool setVal(const double &sat);
        bool setDeltaVal(const double &);

        double getHue() const;
        double getDeltaHue() const;
        double getSat() const;
        double getDeltaSat() const;
        double getVal() const;
        double getDeltaVal() const;
};

