#pragma once

#define R(x,y) ((y*width+x)*3)
#define G(x,y) ((y*width+x)*3+1)
#define B(x,y) ((y*width+x)*3+2)
#define Y(x,y) R(x,y)
#define Cb(x,y) G(x,y)
#define Cr(x,y) B(x,y)
#define A(x,y) (y*width+x)

#define XX 1000
#define YY 10

#define M_PI 3.14159

typedef struct{
    unsigned char r, g, b;
}color;

typedef struct{
    float y;
    char cb, cr;
}colorY;

typedef struct{
    color key_color;
    colorY key_colorY;
    char kg;
    float accept_angle_cos;
    float accept_angle_sin;
    unsigned char accept_angle_tg;
    unsigned char accept_angle_ctg;
    unsigned char one_over_kc;
    unsigned char kfgy_scale;
}info;

class GSChromaKey2
{
    private: 
        int width;
        int height;
        int bg_width, fg_width, bg_height, fg_height, dummy_int;
        time_t start, finish;
        char *fgcb, *fgcr, *bgcb, *bgcr, *resultcb, *resultcr;
        unsigned char *fgy, *bgy, *resulty;
        bool memoryAllocated;

    public:
        cv::Mat finalMask;
        info ck_info;
        cv::Mat fond, devant, result;
        float angle;
        float noise_level;

        GSChromaKey2(cv::Mat fb, cv::Mat bg, float angle, float noise_level);
        ~GSChromaKey2(){
            if (memoryAllocated){
                free(bgy);
                free(bgcr);
                free(bgcb);
                free(fgy);
                free(fgcr);
                free(fgcb);
                free(resulty);
                free(resultcb);
                free(resultcr);
            }
        }
        void setAngle(const float&);
        void prepareInputs(cv::Point point); // ce point correspond � celui dont la couleur sera retir�e sur toute l'image.
        void prepareResult();
        int process(uchar **masque);
        int ck(unsigned char *fgy, char *fgcb, char *fgcr,
            unsigned char *bgy, char *bgcb, char *bgcr,
            unsigned char *resy, char *rescb, char *rescr, info *cki, uchar **masque);
        void get_info(info *cki);
        static float RGB2Y(color *c, colorY *y);
};