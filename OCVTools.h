
static void fillWith(cv::Mat& img, int r, int g, int b){

    uchar* ligne0 = img.ptr<uchar>(0);
    for (int i = 0; i < img.cols; i++){
        ligne0[i*img.channels()] = b;
        ligne0[i*img.channels() + 1] = g;
        ligne0[i*img.channels() + 2] = r;
        if (img.channels())
            ligne0[i*img.channels() + 3] = 255;
    }
    for (int i = 0; i < img.rows; i++)
        memcpy(img.ptr<uchar>(i), ligne0, img.cols*img.channels());
}

static void plaquage(cv::Mat& img, const cv::Mat& over, const unsigned int &opacity){
    if (opacity > 100)
        return;
    float a, b, opDst, opTotal = 1.f;
    int nci = img.channels(), nco = over.channels();

    for (int i = 0; i < over.rows; i++){
        uchar* srcRow = img.ptr<uchar>(i);
        const uchar* ovrRow = over.ptr<uchar>(i);

        if (opacity == 100 && nci == nco && nci == 3)
            memcpy(srcRow, ovrRow, img.cols*nci);
        else{
            for (int j = 0; j < over.cols; j++){
                if (nci == 3 && (nco == 4 || nco == 3)){            // Si on plaque sur une image sans transparence
                    a = opacity / 100.f;
                    if (nco == 4)
                        a *= (ovrRow[4 * j + 3] / 255.f);
                    if (a < 0.01)
                        continue;
                    for (int k = 0; k < 3; k++)
                        srcRow[j * 3 + k] = (uchar)((float)ovrRow[j * nco + k] * a + (float)srcRow[j * 3 + k] * (1.f - a));
                }
                else if (nci == 4 && (nco == 3 || nco == 4)){       // Si on plaque sur une image transparente
                    opDst = srcRow[j * 4 + 3] / 255.f;
                    a = opacity / 100.f;
                    if (nco == 4)
                        a *= ovrRow[4 * j + 3] / 255.f;
                    b = opDst * (1.f - a);
                    if ((a + b) < 0.01)
                        continue;
                    srcRow[j * 4 + 3] = (uchar)((a + b) * 255);
                    for (int k = 0; k < 3; k++)
                        srcRow[j * 4 + k] = (uchar)((float)ovrRow[j * nco + k] * a + (float)srcRow[j * 4 + k] * b);
                }
                else
                    return;
            }
        }
    }
}

static double angleTroisPoints(cv::Point_<float> a, cv::Point_<float> b, cv::Point_<float> c){
    cv::Point_<float> ab(b.x - a.x, b.y - a.y );
    cv::Point_<float> cb(b.x - c.x, b.y - c.y );

    float dot = (ab.x * cb.x + ab.y * cb.y); // dot product
    float cross = (ab.x * cb.y - ab.y * cb.x); // cross product

    float alpha = atan2(cross, dot);

    return -1 * (int)floor(alpha * 180. / 3.14159 + 0.5); // -1 pour etre dans le sens trigo
}

static void plaquageCentre(cv::Mat& img, cv::Mat &over, int opacity = 100){
    plaquage(img(cv::Rect((img.cols - over.cols) / 2,
        (img.rows - over.rows) / 2,
        over.cols,
        over.rows)), over, opacity);
}

static double degToRad(const double &angle){
    double rad = angle * 3.14159 / 180.;
    rad = fmod(rad, 2 * 3.14159);
    if (rad < 0)
        rad += 2 * 3.14159;
    return rad;
}

static double radToDeg(const double &angle){
    double deg = angle * 180. / 3.14159;
    deg = fmod(deg, 360.);
    if (deg < 0)
        deg += 360;
    return deg;
}

static void addAlphaChannel(cv::Mat& img){
    cv::Mat result(cv::Size(img.cols, img.rows), CV_8UC4);
    plaquage(result, img, 100);
    img = result;
}

static void removeAlphaChannel(cv::Mat& img){
    cv::Mat result(cv::Size(img.cols, img.rows), CV_8UC3);
    plaquage(result, img, 100);
    img = result;
}